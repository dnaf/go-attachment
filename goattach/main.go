package main

import (
	"encoding/binary"
	"fmt"
	"io"
	"os"
)

func main() {
	n, err := io.Copy(os.Stdout, os.Stdin)
	if err != nil {
		panic(err)
	}
	if err := binary.Write(os.Stdout, binary.BigEndian, uint64(n)); err != nil {
		panic(err)
	}
	fmt.Fprintf(os.Stderr, "Attachment: %d bytes\n", n)
}
