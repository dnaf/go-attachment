package attachment

import (
	"encoding/binary"
	"errors"
	"io"
	"os"
)

// Data is the data of the attachment. This is initialized by Load()
var Data *AttachmentReader

// Load loads the current binary's attachment into Data.
// If the attachment is already loaded, this does nothing.
func Load() error {
	if Data == nil {
		f, err := os.Open(os.Args[0])
		if err != nil {
			return err
		}
		Data, err = newAttachmentReader(f)
		if err != nil {
			return err
		}
	}
	return nil
}

// AttachmentReader is a wrapper that reads attachment data.
type AttachmentReader struct {
	f      *os.File
	offset int64
	size   int64
	cursor int64
}

func newAttachmentReader(f *os.File) (*AttachmentReader, error) {
	// Get size of attachment by reading a uint64be from end
	var attachmentSize uint64
	{
		if _, err := f.Seek(-8, io.SeekEnd); err != nil {
			return nil, err
		}
		if err := binary.Read(f, binary.BigEndian, &attachmentSize); err != nil {
			return nil, err
		}
	}

	// Get the offset of the attachment by seeking to its start
	offset, err := f.Seek(-8-int64(attachmentSize), io.SeekEnd)
	if err != nil {
		return nil, err
	}

	return &AttachmentReader{
		f:      f,
		offset: offset,
		size:   int64(attachmentSize),
	}, nil
}

func (r *AttachmentReader) Read(p []byte) (n int, err error) {
	n, err = r.ReadAt(p, r.cursor)
	r.cursor += int64(n)
	return n, err
}
func (r *AttachmentReader) Seek(offset int64, whence int) (int64, error) {
	switch whence {
	case io.SeekCurrent:
		offset = r.cursor + offset
	case io.SeekEnd:
		offset = r.size + offset
	}
	if offset < 0 || offset > int64(r.size) {
		return offset, errors.New("out of bounds seek")
	}
	return offset, nil
}

func (r *AttachmentReader) ReadAt(p []byte, off int64) (n int, err error) {
	maxn := len(p)
	if int64(len(p))+off > r.size {
		maxn = int(r.size - off)
	}
	n, err = r.f.ReadAt(p[:maxn], r.offset+off)
	if err != nil {
		return n, err
	}
	if int64(n)+off == r.size {
		err = io.EOF
	}
	return n, err
}
